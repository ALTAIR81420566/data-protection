package fileManag;

import decoder.alphabet.FrequencyAlphabet;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by altair on 26.09.16.
 */
public class TextAnalyzer {
    private double amountChars;

    public HashMap<Character, Double> makeFrequencyInputChar(String inputText, FrequencyAlphabet frequencyAlphabet){

        HashMap<Character, Integer> NumberInputChar = getNumberInputChar( inputText , frequencyAlphabet);
        HashMap<Character, Double> frequencyInputChar = new HashMap<>();
        Map.Entry<Character, Integer> t;
        double value = 0;
        Iterator<Map.Entry<Character, Integer>> iterator = NumberInputChar.entrySet().iterator();
        while(iterator.hasNext()){
            t = iterator.next();
            value =  t.getValue() / amountChars;
            frequencyInputChar.put(t.getKey(), value);
        }

        return  frequencyInputChar;
    }

    //Возвращает количество вхождений для каждой буквы
    // и присваевает amountChars общее количество букв
    private HashMap<Character, Integer> getNumberInputChar(String inputText, FrequencyAlphabet frequencyAlphabet){
        Pattern pattern = Pattern.compile(frequencyAlphabet.getRegular());
        Matcher matcher;
        HashMap<Character, Integer> numberInputChar = new HashMap<>();


        for (int i = 0; i < inputText.length(); i++){
            char key = inputText.charAt(i);
            matcher = pattern.matcher(Character.toString(key));
            if (matcher.matches()){
                if(numberInputChar.containsKey(key)){
                    int value = numberInputChar.get(key) + 1;
                    numberInputChar.put(key, value);
                }else{
                    numberInputChar.put(key, 1);
                }
                amountChars++;
            }
        }
        System.out.println(numberInputChar);
        return numberInputChar;
    }
}
