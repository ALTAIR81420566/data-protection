package fileManag;

import java.io.*;

/**
 * Created by altair on 26.09.16.
 */
public class FileManager {

    public String readFile(String input){
        StringBuilder inputText = new StringBuilder();
        String inputLine = null;
        try(BufferedReader br = new BufferedReader (new FileReader(input))) {

            while ((inputLine = br.readLine()) != null){
                inputText.append(inputLine).append("\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return inputText.toString().toLowerCase();
    }

    public void writeFile(String output, String text){
        try(BufferedWriter bufferedWriter =  new BufferedWriter(new FileWriter(output))) {

            bufferedWriter.write(text);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
