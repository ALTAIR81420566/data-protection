package decoder.alphabet;

/**
 * Created by altair on 24.09.16.
 */
public interface FrequencyAlphabet {

   public String[][] getFrequencyChar();

   public String getRegular();
}
