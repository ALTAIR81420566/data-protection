package decoder.alphabet;


import fileManag.FileManager;
import fileManag.TextAnalyzer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by altair on 24.09.16.
 */
public class FrequencyForFile implements FrequencyAlphabet {
    private static  String frequencyChar[][] = {};
    private String input;
    private String inputText;


    public String[][] getFrequencyChar() {
        return frequencyChar;
    }

    public String getRegular() {

        return "[A-Za-zА-Яа-я]";
    }

    public FrequencyForFile(String input){
        this.input = input;
        makeFrequencyChar();
    }

    private void makeFrequencyChar(){
        String frequencyChar[][];
        FileManager fileManager =  new FileManager();
        this.inputText = fileManager.readFile(this.input);

        HashMap<Character, Double> frequencyInputChar = new HashMap<>();
        TextAnalyzer textAnalyzer =  new TextAnalyzer();
        frequencyInputChar.putAll(textAnalyzer.makeFrequencyInputChar(inputText ,this));
        frequencyChar =  new String[frequencyInputChar.size()][2];

        int i = 0;
        Map.Entry<Character, Double> t;
        Iterator<Map.Entry<Character, Double>> iterator = frequencyInputChar.entrySet().iterator();

        while (iterator.hasNext()){
            t = iterator.next();
            frequencyChar[i][0] = t.getKey().toString();
            frequencyChar[i][1] = t.getValue().toString();
            i++;
        }


        this.frequencyChar = frequencyChar;
    }
}
