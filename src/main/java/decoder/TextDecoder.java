package decoder;

import decoder.alphabet.FrequencyAlphabet;
import fileManag.FileManager;
import fileManag.TextAnalyzer;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by altair on 24.09.16.
 */
public class TextDecoder {
    private FrequencyAlphabet frequencyAlphabet;
    private HashMap<Character, Double> frequencyInputChar = new HashMap<>();
    private String inputText;
    private String outputText;
    private String input;
    private String output;

    public TextDecoder(FrequencyAlphabet frequencyAlphabet,String input, String output){
        this.frequencyAlphabet = frequencyAlphabet;
        this.input = input;
        this.output = output;
    }

    public void SetFrequencyAlphabet(FrequencyAlphabet frequencyAlphabet){
        this.frequencyAlphabet = frequencyAlphabet;

    }

    public void start(){
        FileManager fileManager =  new FileManager();
        this.inputText = fileManager.readFile(this.input);
        this.outputText = inputText;
        makeFrequencyInputChar();
        decode();
        fileManager.writeFile(output,outputText);
    }

    private void decode(){
        System.out.println(Arrays.toString(frequencyAlphabet.getFrequencyChar()));
        Map.Entry<Character, Double> t;
        Iterator<Map.Entry<Character, Double>> iterator = frequencyInputChar.entrySet().iterator();
        int indexConfirmity = -1;
        double values = 0.0;
        System.out.println(frequencyInputChar);
        while(iterator.hasNext()){
            t = iterator.next();
            double maxConfotmity = 1.0;
            for (int i = 0; i < frequencyAlphabet.getFrequencyChar().length; i++){
                    values = Double.parseDouble(frequencyAlphabet.getFrequencyChar()[i][1]) - t.getValue();
                    values = values > 0 ? values : values * -1;
                    if(maxConfotmity > values){
                        maxConfotmity = values;
                        indexConfirmity = i;
                    }
            }
            outputText = outputText.replaceAll(t.getKey().toString(),frequencyAlphabet.getFrequencyChar()[indexConfirmity][0]);
        }

    }

    private void makeFrequencyInputChar(){
        TextAnalyzer textAnalyzer =  new TextAnalyzer();
        frequencyInputChar.putAll(textAnalyzer.makeFrequencyInputChar(inputText ,frequencyAlphabet));
    }


    private void writeFile(String output){
        
    }

    public String getInput(){
        return input;
    }

    public void setInput(String input){
        this.input = input;
    }

    public String getOutput(){
        return output;
    }

    public void setOutput(String output){
        this.output = output;
    }

    public String getOutputText() {
        return outputText;
    }

}
